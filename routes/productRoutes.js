const express = require('express')
const router = express.Router()
const productController = require("../controllers/productController")
const auth = require ('../auth.js');
const product = require('../models/product');



//USER ENDPOINT
// endpoint for getting all available products
router.get('/', productController.getAvailableProducts);

//ENDPOINT FILTER PRODUCTS
router.get('/filter', productController.getFilteredProducts);

//USER ENDPOINT
//endpoint for adding product to cart
router.patch('/addtocart/', auth.verify, productController.addToCart);


//ADMIN ENDPOINT
// endpoint for getting all products
// router.get('/allproducts', auth.verify, productController.getAllProducts);
router.get('/allproducts', auth.verify, productController.getAllProducts);
//ADMIN ENDPOINT
// endpoint for adding product
router.post('/addproduct', auth.verify, productController.addProduct);
// Pang debug
// router.post('/addproduct', productController.addProduct);
//ADMIN ENDPOINT
//endpoint for archiving product
router.patch('/archiveproduct/:productId', auth.verify, productController.archiveProduct)

//ADMIN ENDPOINT
//endpoint for putting product on sale
router.patch('/putonsale', auth.verify, productController.putOnSale);

//ADMIN ENDPOINT
// endpoint for UPDATING product
router.put('/updateproduct/:productId', auth.verify, productController.updateProduct);

//USER ENDPOINT
// endpoint for finding a product via Id
router.get('/:productId', productController.getProduct);

module.exports = router;