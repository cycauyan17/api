const Product = require('../models/product');
const router = require('../routes/productRoutes');
const auth = require('../auth');

const { update } = require('../models/product');
const User = require('../models/user');
const { response } = require('express');


module.exports.addProduct = async (req, res)=>{
    const userData = auth.decode(req.headers.authorization);
// 
    let newProduct = new Product({
        prodName: req.body.prodName,
        price: req.body.price,
        description: req.body.description,
        specs:{
            dimensions:req.body.specs.dimensions,
            color: req.body.specs.color,
            size: req.body.specs.size
        },
        stocks:req.body.stocks,
    })

    if(userData.isAdmin){

        await newProduct.save()
            .then((result)=>{
                res.status(201).json({product:result})
            })
            .catch(error=>{
                res.status(400).send(error);
            })
    }
    else{
        res.status(403).json({error:`You do not have admin privileges.`})
    }

}


module.exports.getAvailableProducts = async (req,res)=>{
    let searchKeyWord = req.body.prodName;

    if (searchKeyWord){

    let searchProduct = await Product.find( { 'prodName' : { '$regex' : searchKeyWord , '$options' : 'i' },isAvailable:true },{ prodName: 1, price: 1, description: 1, specs : 1, stocks: 1, isOnsale:1, discountedPrice:1} ).sort({prodName:1}).catch(error=>{res.send(error)})

        if(searchProduct.length < 1){
            return res.status(200).json({message:'No available products at the moment.'});
        }
        return res.status(200).json({message:`Search complete:`, found:searchProduct.length, keyword:req.body.prodName , products: searchProduct });
    }

    let getAvailable = await Product.find({isAvailable:true}, { prodName: 1, price: 1, description: 1, specs : 1, stocks: 1, isOnsale:1, discountedPrice:1}).sort({prodName:1}).catch(error=>res.status(400).json({error:error}))
   
        if(getAvailable.length < 1){
            return res.status(200).json({message:'No available products at the moment.', count:getAvailable.length});
        }

        return res.status(200).json({count:getAvailable.length, products:getAvailable});
}


module.exports.getFilteredProducts = async (req,res)=>{
    let filter = {};
    Object.assign(filter, req.body);//test
    console.log(filter);//needs test
    if (req.body.prodName){

        let productsFound = await Product.find( { 'prodName' : { '$regex' : req.body.prodName , '$options' : 'i' }, isAvailable:true },{ prodName: 1, price: 1, description: 1, specs : 1, stocks: 1, isOnsale:1, discountedPrice:1} ).sort({prodName:1}).catch(error=>{
            console.log(error);
            res.send(error);
        })
            
            if(productsFound.length < 1){
                return res.status(200).json({message:'No available products at the moment.', count:productsFound.length});
            }
            
            return res.status(200).json({message:`Search complete:`,count:productsFound.length, products: productsFound });
        }

    await Product.find(filter, { prodName: 1, price: 1, description: 1, specs : 1, stocks: 1, isOnsale:1, discountedPrice:1}).sort({prodName:1})
    .then(result =>{
        if(result.length < 1){
            return res.status(200).json({message:'No product(s) found.', count:result.length})
        }
        return res.status(200).json({message:`Search complete:`,count:result.length, products: result });
    })
    .catch(error=>res.send(error))
}


// used for purchasing products
module.exports.getProduct = async (req,res)=>{
    let productId = req.params.productId;

    await Product.findById(productId)
    .then(result =>{
        console.log(`This is result`,result);
        if (result){
            // return res.status(200).json({product:result});
            return res.send(result);
        }
        else{
            return res.status(200).json({message:'No match found', count:0});
        }
        })
    .catch(error=>res.send(error))
}



module.exports.getAllProducts = async (req,res)=>{
    const userData = auth.decode(req.headers.authorization)

    // if (userData.isAdmin){

    await Product.find({})
    .then(result =>{

        if(result.length<1){
            return res.status(200).json({message:'No Products found.', count:result.length})
        }

        return res.status(200).json({count:result.length, products:result})})
    .catch(error=>res.send(error))
    // }
    // else{
    //     res.status(403).json({message:"You do not have admin privileges."})
    // }

}



module.exports.updateProduct = async (req,res) =>{
    
    const userData = auth.decode(req.headers.authorization);
    const productId = req.params.productId;
    console.log(userData);

    let updatedProduct = {
        prodName: req.body.prodName,
        price: req.body.price,
        description: req.body.description,
        category: req.body.category,
        specs:{
            dimensions:req.body.specs.dimensions,
            colors: req.body.specs.color,
            size: req.body.specs.size
        },
        stocks:req.body.stocks
    }

    if (userData.isAdmin){
        await Product.findByIdAndUpdate(productId, updatedProduct, {new:true})
        .then(result=>{
             return res.status(200).json({product:result})
        })
        .catch(err=>{
           return res.status(400).json({error:err})
        })
    }
    else{
        return res.status(403).json({error:"You do not have admin priveleges.", isAdmin:false})
    }

}


module.exports.archiveProduct = async (req,res) =>{
    const userData = auth.decode(req.headers.authorization);
    const productId = req.params.productId

    if (userData.isAdmin){

        await Product.findById(productId)
          .then(result=>{
                let update = !result.isAvailable;
            
            return Product.findByIdAndUpdate(productId, {isAvailable : update}, {new:true})
                    .then(result => {
                        result.password = "Confidential";
                        res.status(200).json({product:result})
                    })
          })
          .catch(err => res.status(400).json({error:err}))

    }
    else{
        return res.status(403).json({error:"You do not have admin priveleges.", isAdmin:false})
    }

}



module.exports.putOnSale = async (req,res) =>{
    
    const userData = auth.decode(req.headers.authorization);
    // const productId = req.query.productId;
    const productId = req.body.productId;
    // const discquery = Number(req.query.discount);
    const discquery = Number(req.body.discount);
    const discount = Number(discquery) * .01 ;
    let originalPrice;
    let notif = "";

    console.log(req.query)

    let updatedProduct = {
        isOnsale: true,
        discountedPrice: 0
    }

    if(userData.isAdmin && (discquery>1 && discquery<101)){

        //find ID return found product update product
        await (Product.findById(productId)).then(product=>{
         
            let update = !(product.isOnsale)

            updatedProduct.isOnsale = update

            originalPrice = product.price
            

            if(updatedProduct.isOnsale){
                notif = `${product.prodName} product was put on 'Sale'`
                const discountedprice = originalPrice - (discount * originalPrice);
                updatedProduct.discountedPrice = discountedprice;
                console.log(`Original price was:${originalPrice}`);
                console.log(`Discounted price was updated to ${updatedProduct.discountedPrice}`);
            }
            else{
                notif = `${product.prodName} product has been removed from 'Sale'.`
                // console.log(`${product.prodName} product has been removed from 'Sale'.`)
                updatedProduct.discountedPrice = 0;
            }
            
        }).then(()=> {return updating()})
        .catch(error=>{
            console.log(error);
            res.send(error);
        })
    }
    else if(discquery <0 || discquery>100){
        // 405 Method Not Allowed: The request method is known by the server but is not supported by the target resource.
        return res.status(405).json({error:`Discount must be within 1 to 100`});
    }
    else{
        return res.status(403).json({error:"You do not have admin priveleges.", isAdmin:false});
    }

    async function updating(){
        if (userData.isAdmin){
  
            await Product.findByIdAndUpdate(productId, updatedProduct, {new:true})
            .then(result=>{
                return res.status(200).json({message:notif, product:result})
            })
            .catch(err=>{
            return res.status(400).json({error:err})
            })
        }
        else{
            return res.status(403).json({message:"You do not have admin priveleges.", isAdmin:false})
        }
    }
}



module.exports.addToCart = async (req,res)=>{
    let userData = auth.decode(req.headers.authorization)

    let product = {
        productId : req.body.prodId,
        quantity : req.body.quantity,
        subTotal: 0
    }

    // Business Logic:
    /*
        1. Add to cart product input from body
        2. Properties of the added item to cart shall contain id, quantity and subtotal
        3. update cart of the user so that it can still be viewed even if they logged out.

    */
    if(!userData.isAdmin){

            if (product.quantity<1){
                return res.status(405).json({message:'Invalid quantity.'})
            }
        
            await Product.findById(product.productId).then(result =>{
                console.log(result);
                if(result!==null){
                        if (result.isOnsale){
                            product.subTotal = result.discountedPrice * product.quantity
                            //  console.log(`Fetched product details.`)
                        }
                        else{
                            product.subTotal = result.price * product.quantity
                        } 
                            //  console.log(`Fetched product details.`)
                        User.findById(userData.id).then(cartOwner =>{

                            let found = false;
                            for (let i = 0; i < cartOwner.shoppingCart.length; i++) {
                                if (cartOwner.shoppingCart[i].productId.equals(product.productId)) {
                                    cartOwner.shoppingCart[i].quantity += product.quantity;
                                    cartOwner.shoppingCart[i].subTotal += product.subTotal;
                                    found = true;
                                    
                                    break;
                                }
                            }
                            if (!found) {
                                cartOwner.shoppingCart.push(product);
                            }
                                console.log(`found ${found}`);
                                console.log(product);
                                cartOwner.save();
                                console.log(cartOwner.shoppingCart)
        
                            return res.status(200).json({
                                count: cartOwner.shoppingCart.length,
                                "Cart Contents": cartOwner.shoppingCart});
                            })  
                    }
                else{
                    return res.status(400).json({message:`No Product found.`});
                }
                 
            })
            .catch(error => res.status(400).json({error:error}))
        
    }
    else{
        return res.status(403).json({message: `You must login first.`, isAdmin:true})
    }   
}
            



