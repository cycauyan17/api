const mongoose = require ('mongoose')
// const Product = require('../models/product')


const userSchema = new mongoose.Schema({

        firstName: {
            type: String,
            uppercase: true,
            required: [true, "Firstname is required"]
        },
        middleName: {
            type: String,
            uppercase:true
        },
        lastName: {
            type: String,
            uppercase:true,
            required: [true, "Firstname is required"]
        },
        isAdmin:{
            type:Boolean,
            default: false
        },
        contacts:{
            mobileNum:{
                type:String
            },
            email:{
                type: String,
                trim: true,
                lowercase: true,
                unique: true,
                required: [true, "Email required"]
            }
        },
        password:{
            type:String,
            minLength: 8,
            required: [true, "Password is required"]
        },
        address:{
          
            province:{
                type:String,
                uppercase:true,
                required: [true, "Province is required"]
            },
            municipality: {
                type: String,
                uppercase:true,
                required: [true, "Municipality is required"]
            },
            barangay:{
                type: String,
                uppercase:true,
                required: [true, "Barangay is required"]
            },
            houseNum: {
                type:String,
                uppercase:true,
                required: [true, "Put n/a if not applicable"]
            },
            street:{
                type:String,
                uppercase:true,
                required: [true, "Put n/a if not applicable"]
            },
            zipcode:{
                type: String,
                required: [true, "Zip Code is required"]
            }
        },
        shoppingCart:[{
            productId:{
                type: mongoose.Schema.Types.ObjectId, 
                ref: 'Product',
                required: [true, "productId is required"]
            },
            quantity:{
                type:Number,
                required: [true, "Quantity is required"]
            },
            subTotal:{
                type:Number,
                required:true
            }
        }],
        userTransactions:[{
            status:{
                type: String,
                required: [true, "status is required"],
            },
            transactionDate:{
                type: Date,
                default: new Date()
            },
            orderId:{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Order',
                required:[true, "orderId is required"]
            }
        }], 

},{
    toJSON: {virtuals: true}
},)

userSchema.set('timestamps', true);
module.exports = mongoose.model("User", userSchema)


//display a virtual property, put this ins model
userSchema.virtual('totalamt').get(function () {
    let gt =0;
    for (let i = 0; i<this.shoppingCart.length; i++){
        gt += this.shoppingCart[i].subTotal
    }

    return gt

})