const mongoose = require('mongoose')


const orderSchema = new mongoose.Schema({
    // orderNum:{
    //     type:Number,
    //     //must auto Increment
    // },
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required: true
    },

    productsOrdered:[{
        productId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product',
            required: [true, "productId is required"]
        },
        quantity:{
            type:Number,
            required: [true, "Quantity is required"]
        },
        subTotal:{
                type:Number,
                required:true
        }
    }],
    status:{
        type:String,
        lowercase: true,
        default: "pending" 
    },
    totalAmount:{
        type:Number,
        required: true
    }
})
orderSchema.set('timestamps', true);
module.exports = mongoose.model('Order', orderSchema);



